#!/bin/bash
#
# Pre-commit hook to update the README based on what is in the PKGBUILD
#

source PKGBUILD

sed -i "s,^:Version: .*,:Version: ${pkgver},;s,^:Date: .*,:Date:   $(date +%Y-%m-%d)," README.rst

git add README.rst
