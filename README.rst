==========
autotunnel
==========

---------------------------------------------------------
Easily create tunnels, reverse tunnels, and SOCKS proxies
---------------------------------------------------------

:Author: Josh VanderLinden <arch@cloudlery.com>
:Date:   2012-10-20
:Copyright: Public Domain
:Version: 0.3.4
:Manual section: 1
:Manual group: System Tools

Synopsis
========

    autotunnel [-k] [profile]

Description
===========

This program is intended to make it easy to establish a single or multiple
tunnels between your machine and another machine running SSH. It also has the
ability to create reverse tunnels and setup a SOCKS proxy.

One interesting option offered by ``autotunnel`` is that of profiles. You may
have multiple profiles of tunnels, each to different systems with different
settings.

There are two ways to call ``autotunnel``:

#. to launch and create the tunnels
#. to kill an existing set of tunnels for a given profile

Options
=======

[-k]
  Kill the specified profile. If the profile is omitted, the default profile
  will be killed.

[profile]
  The name of the profile to launch or kill. If omitted, the default profile
  will be used.

Tunnel Profiles
===============

``autotunnel`` allows you to setup tunnels to different remote systems by using
profiles. Profiles live in ``/etc/autotunnel.d``, and must have the extension
``.conf``. The ``default`` profile is ``/etc/autotunnel.d/default.conf``. To
create another profile, simply copy the default profile or create a new a new
file with the following settings::

    cp /etc/autotunnel.d/default.conf /etc/autotunnel.d/new-profile.conf

There are a few configuration values that you'll be particularly interested in.

* ``USER``: This is the username you wish to login as on the remote system.
* ``HOST``: This is the hostname or IP of the remote system.
* ``SSH_PORT``: This is the port that you use to connect to SSH on the remote
  system. Defaults to ``22``.
* ``SOCKS_PORT``: This is the port to establish a SOCKS proxy on. If set to
  ``0``, no SOCKS proxy will be established.
* ``TUNNELS``: This is a list of tunnels that will allow you access ports on
  the remote system via ports on your local system. See the next section for
  details.
* ``REVERSE``: This is a list of reverse tunnels that will allow you to access
  ports on your local system via ports on the remote system. See the next
  section for details.
* ``AUTOSSH_PORT``: This is the port that is used to determine when tunnels are
  active and when they need to be re-established.

If you're using multiple profiles, be sure to update the ``AUTOSSH_PORT`` value
in each profile so there's no conflict. This is the port that is used to
determine if a profile is already running or not. If two profiles have the same
``AUTOSSH_PORT``, only one of the two profiles may be running at one time.

About Tunneling
===============

Tunnels are very powerful. They allow you to access remote systems by using
local ports. Likewise, reverse tunnels allow remote systems to access your
system by using open ports on the remote system itself.

The profile template allows you to configure both of these types of tunnels.

Normal Tunnels
--------------

You can configure your normal tunnels by specifying them in the ``TUNNELS``
array of your profile. Again, normal tunnels allow you to access ports on
remote systems by using local ports on your system. They're formatted as::

    [local port]:[host]:[remote port]

The most basic type of tunnel forwards a port on your remote system to the same
port on your local system. A simple case is when you want to access MySQL
running on a remote server. A tunnel like::

    3306:localhost:3306

allows you to access MySQL on the remote server as though it were running
locally.

Sometimes you may wish to create tunnels that need to use a different local
port for whatever reason (usually that the port is already used by something
else). The following will create a tunnel to a remote host that allows you to
access remote port 3389 (RDP) on local port 33890 and to access remote port
5900 (VNC) on local port 59000.

::

    TUNNELS=(
      '33890:localhost:3389'
      '59000:localhost:5900'
    )

Reverse Tunnels
---------------

Reverse tunnels allow you to access ports on your local machine by accessing
open ports on the remote host when you're logged in on that system. This is a
great way to create your own little VPN of sorts. It sometimes takes a few
minutes to wrap your head around how reverse tunnels work. They're formatted
as::

    A:[host]:B

Where A is the port that will open up on the remote system, which will grant
access to port B on the local system.

As another example, say your local system is "PC A" and your remote system is
"PC B". When logged into PC B (either normally or via SSH), the following
allows you to SSH into PC A. One would simply run a command like ``ssh
pc_a_user@localhost -p 2222`` on PC B.

::

    REVERSE=(
      '2222:localhost:22'
    )

Slightly More Advanced Tunnels
------------------------------

There are a lot of neat things you can do with tunnels. I will leave it up to
you to Google the more advanced things, but I would just like to mention
something. You may have noticed that all of the above examples have
``localhost`` in them. This is not absolutely necessary--you can be the
hostname or IP of another system on the network.

For example, say I have 3 PCs. PC A is my machine at work. PC B is my laptop at
home. PC C is my desktop at home. My home router is configured to forward all
traffic to PC B, but I want to access PC C from PC A.

To do this, I can setup a normal tunnel to PC B as such::

    2222:pc_c:22

Then I can SSH into PC C from PC A by issuing a command like::

    ssh pc_c_user@localhost -p 2222

SOCKS Proxies
=============

SOCKS proxies are a way for you to proxy network traffic through an SSH
connection. This is great because the traffic is encrypted, and it allows you
to bypass things like stupid corporate Internet filters.

To establish a SOCKS proxy, configure your profile's ``SOCKS_PORT`` to
something greater than 0. After that, it's just a matter of configuring your
computer or browser to use the SOCKS proxy. Each window manager and browser
will have its own configuration. Another Google assignment for you!

Start Tunnels Automatically At Boot
===================================

It's very important that you setup pre-shared key (PSK) authentication between
your local system and the remote systems you plan to establish tunnels with if
you want the tunnels to activate at boot time. You'll want to setup PSK
authentication for your local ``root`` user to the remote system(s).
Alternatively, you may edit the ``systemd`` unit files or ``supervisord``
configuration files to launch the tunnels as a user of your choice.

Setting up pre-shared key authentication is easy to do, and the Internet will
be of much help to you if you're unfamiliar with it. However, I'll give a quick
overview. As root, type this command::

    ssh-keygen

Accept all defaults. Now copy your public key to the remote host. Again, as
root, run this command::

    ssh-copy-id remote_user@remote_host

Obviously, replace ``remote_user`` and ``remote_host`` with the appropriate
information. Then enter the password for ``remote_user`` on ``remote_host``. If
all went well, you should now be able to SSH into ``remote_host`` as
``remote_user`` without entering a password. If not, Google is your friend.

systemd
-------

You have two options for starting the tunnels automatically at boot time. The
first, and recommended solution, is to use ``systemd``. All you need to do is
run the following command as root, and the tunnels defined in your default
profile will be established the next time you reboot.

::

    systemctl enable autotunnel

If you wish to have a specific tunnel profile launch at boot time, simply use
the template unit file::

    systemctl enable autotunnel@profile

supervisord
-----------

If you're using ``supervisord``, you should be able to just copy or symlink the
default configuration to your ``/etc/supervisor.d`` directory::

    cp /etc/autotunnel.d/autotunnel.ini /etc/supervisor.d/

If you wish for other profiles to be launched automatically, copy this file and
replace this line::

    command=/usr/local/bin/autotunnel default

With::

    command=/usr/local/bin/autotunnel specific-profile

Downloading
===========

Official downloads live on BitBucket at
https://bitbucket.org/codekoala/autotunnel/downloads and in `Arch Linux`_'s
`AUR`_. While this project was built for and tested on `Arch Linux`_, it is
just a bunch of bash scripting on top of ``autossh``. You should be able to run
this anywhere bash and autossh run.

Reporting Bugs
==============

Please report bugs on the official issue tracker at
https://bitbucket.org/codekoala/autotunnel/issues

.. _Arch Linux: http://www.archlinux.org/
.. _AUR: https://aur.archlinux.org/packages.php?O=0&K=autotunnel&do_Search=Go
